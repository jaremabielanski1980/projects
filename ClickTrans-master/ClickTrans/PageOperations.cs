﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace ClickTrans
{
    class PageOperations
    {

        public void EnterString(IWebElement element, string text)
        {
          element.SendKeys(text);
        }

        public void ClickOnElement(IWebElement element)
        {
          element.Click();
        }

    }
}
