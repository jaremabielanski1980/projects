﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;



namespace ClickTrans
{
    class PageObjects
    {

        public PageObjects(IWebDriver drv)
        {
            PageFactory.InitElements(drv, this);
        }

        [FindsBy(How = How.Id, Using = "user_register_company_name")]
        public IWebElement CompanyName { get; set; }

        [FindsBy(How = How.Id, Using = "user_register_email")]
        public IWebElement Email { get; set; }

        [FindsBy(How = How.Id, Using = "user_register_name")]
        public IWebElement Names { get; set; }
    
        [FindsBy(How = How.Id, Using = "user_register_phoneCode")]
        public IWebElement PhoneCode { get; set; }

        [FindsBy(How = How.Id, Using = "user_register_phone")]
        public IWebElement Phone { get; set; }

        [FindsBy(How = How.Id, Using = "user_register_plainPassword")]
        public IWebElement Psswd { get; set; }

        [FindsBy(How = How.Id, Using = "user_register_settings_agreementRegulations")]
        public IWebElement CheckboxRegulations { get; set; }

        [FindsBy(How = How.Id, Using = "user_register_settings_agreementPersonalData")]
        public IWebElement CheckboxRodo { get; set; }

      [FindsBy(How = How.Id, Using = "user_register_submit")]
      public IWebElement SubmitBtn { get; set; }
    

  }
}
