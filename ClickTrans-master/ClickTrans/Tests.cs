using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;


namespace ClickTrans
{

    class Tests
  {

        static void Main(string[] args) {}
        
        static IWebDriver drv = new ChromeDriver();
        PageObjects po = new PageObjects(drv);
        PageOperations pOper = new PageOperations();
 
        [SetUp]
        public void TestPreparation()
        {
        drv.Navigate().GoToUrl("https://dev-1.clicktrans.pl/register-test/courier");
        drv.Manage().Window.Maximize();
        }

        [Test]
        public void ContactFormTest()
        {

        WebDriverWait wait = new WebDriverWait(drv, TimeSpan.FromSeconds(5));
        wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(By.Id("user_register_company_name")));

        pOper.EnterString(po.CompanyName, "ClickTrans Test");
        pOper.EnterString(po.Email, "click@trans.pl");
        pOper.EnterString(po.Names, "ClickTrans Company");
        pOper.EnterString(po.Phone, "+48 123 123 123");
        pOper.ClickOnElement(po.PhoneCode);
        pOper.EnterString(po.PhoneCode, "(+48) Polska" + Keys.Enter);
        pOper.EnterString(po.Psswd, "AutomatedTest2!");
        pOper.ClickOnElement(po.CheckboxRegulations);
        pOper.ClickOnElement(po.CheckboxRodo);
        pOper.ClickOnElement(po.SubmitBtn);

        //!!!!!!!!!! Proper xpath / cssselector should be entered here below
        string notice = drv.FindElement(By.XPath("//*[@id=\"user_register_submit\"]")).Text;

        Assert.IsTrue(drv.PageSource.Contains(notice), "OK - some registration logic is mocked");

        }

        [TearDown]
        public void TestFinalization()
        {
          
          // commented to visualize the result in the web browser
          //drv.Quit();

        }
    }
}
